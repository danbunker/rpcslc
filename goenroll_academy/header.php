<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Academy Enrollment Form 2012</title>
	<link href="/css/2011.css" media="screen" rel="Stylesheet" type="text/css" />
	<!--[if IE]>
	        <link rel="stylesheet" type="text/css" href="./css/IE-2011.css" />
	<![endif]-->
	<link href="/css/jmenu.css" media="screen" rel="Stylesheet" type="text/css" />
	<link rel="stylesheet" href="./css/validationEngine.jquery.css" type="text/css"/>
	<link rel="stylesheet" href="./css/template.css" type="text/css"/>
	<script src="./jquery-1.6.min.js" type="text/javascript"></script>
	<script src="/js/jquery.cycle.all.min.js" type="text/javascript"></script>
	<script src="/js/jmenu.js" type="text/javascript"></script>

	<script src="./languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8">
	</script>
	<script src="./jquery.validationEngine.js" type="text/javascript" charset="utf-8">
	</script>

	<link rel="stylesheet" href="./css/jquery.ui.all.css">
	<script src="./jquery.ui.core.js"></script>
	<script src="./jquery.ui.widget.js"></script>

	<script src="./jquery.ui.datepicker.js"></script>

	<script language="JavaScript">
		$(document).ready(function() {
		    $('#jmenu').jmenu();
		});
	</script>
</head>
<body style="background-color: #2B2922;">
	<div id="page">
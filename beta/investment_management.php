<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

        <div id="corpus">
            <img src="images/inside_page_header.jpg" alt="banner" />
            <div id="content">
                <div class="tanBox" style="padding: 0;">
                    <div class="transBox fr editable" id="serviceBox">
                        <h5>Investment Management</h5>
                        <p>Our mission is to "increase the retirement readiness of employees and employers of small businesses"! Investment Management is the engine that helps us reach that goal.</p>
                        <p>The following is from one of our semi-annual newsletters (January 2010), and essentially describes our investment management theory. In the end, we believe in building customized, well diversified, suitable portfolios and then monitoring them in the context of your life and your goals. Feel free to E-mail us and we will be happy to send you the complete set of these newsletters.</p>
                        <p><em>"You Always Don’t Know" </em></p>
                        <p><em>A few weeks ago I was sitting in a class and the instructor was telling a story and said "I guess you always don’t know what is going to happen". It caught my attention because I think he meant to say "I guess you don’t always know what is going to happen." That is when it hit me. You really "Always don’t know" what is going to happen when it comes to the markets. </em></p>
                        <p><em>If you think about it, the stock market is a valuation mechanism. People and institutions from all over the world gather each work day and judge a fair current price for the future earnings stream of publicly traded companies. Therefore any sharp rise or fall of a single stock price or the market as a whole could represent a surprise. If investors knew what was going to happen tomorrow or next month, they would act today so as to make the event profitable. The problem is that you "Always don’t know". This is a hard concept for some to swallow for two reasons. First, it is usually pretty easy to look back and explain why the sharp turn occurred. This gives us the feeling that it was foreseeable prior to it happening. Second, the few lucky investors that happened to be on the right side of the surprise tell everyone that they knew it was going to happen. This makes us feel like some people knew; we just weren’t one of them. </em></p>
                        <p><em>A few recent events are worth mentioning including: The meteoric rise of stock prices from mid 1998 to early 2000, the subsequent tech bubble burst, September 11th, the rebound in 2003, the collapse from October 2008 through March 9th 2009 and now the unbelievable rebound from 6,547. We may be tempted to think that the last 10 years have held more such surprises than any other decade in the last 100 years but a close look at a long term chart of the markets reveals that there have been other decades with similar swings (like the 70’s). </em></p>
                        <p><em>Here is what I have learned: "you <strong>ALWAYS</strong> don’t know". Even though people come on TV one after another and tell us what just happened and what they think will happen and even though we think we have it figured out based on science, voodoo or whatever, we <strong>ALWAYS</strong> don’t know. </em></p>
                        <p><em>So what does all this mean to investors? In my mind, it means that we must resist the urge to make investment decisions based on the emotions that come from the false premise that we know what is going to happen. We must take a systematic approach using strategies that are designed to help over the long run and include ideas like relative value, suitability, time horizon, dollar cost averaging, rebalancing and diversification. Thankfully, there are more investment tools than ever to help investors manage risk and be diversified. </em></p>
                        <p>written by Jere El-Bakri</p>
                    </div>
<?php include 'includes/servicesNav.php' ?>
                    <div class="extender"></div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(function () {
                $("#main-img").cycle({
                    fx: 'fade',
                    timeout: 10000
                });
            });
        </script>


<?php include 'includes/footer.php' ?>

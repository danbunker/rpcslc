<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

		<div id="corpus">
			<img src="images/inside_page_header.jpg" alt="banner" />
			<div id="content">
				<div class="bio single editable">
                    <img src="images/bio/Alli-Pic-xl.png" alt="Alli Tidwell" class="fl" />
                    <p class="name">Alli Tidwell</p>
                    <p class="title">Client Services Assistant</p>

                    <p>Alli Tidwell is a Client Services Assistant at Retirement Plan Consultants, Inc.  She is an integral part of our team, and helps us to provide systematic service to plan sponsors and plan participants.     </p>
                    <p>Prior to joining our firm, Alli had 2 years of experience working in the PEO Industry, where she worked with HR, benefits, and payroll services for small businesses and their employees. She graduated with a BA in English from the University of Utah with honors. Alli has earned both a Specialist Degree and Associates Degree through the LPL Admin U program.  She continues to seek out opportunities to expand her knowledge and provides great service to our clients.</p>

                    <p class="readMore"><a href="mailto:alli@rpcslc.com">Contact Alli...</a></p>
				</div>
			</div>
			<div class="extender"></div>
		</div>


<?php include 'includes/footer.php' ?>

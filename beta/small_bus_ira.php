<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

        <div id="corpus">
            <img src="images/inside_page_header.jpg" alt="banner" />
            <div id="content">
                <div class="tanBox" style="padding: 0;">
                    <div class="transBox fr editable" id="serviceBox">
                        <h5>Small Business IRAs</h5>
                        <p>Our mission is to “increase the retirement readiness of employees and employers of small businesses”! Small business IRAs are one of the tools we use to accomplish that goal.</p>
                        <p>You may have heard of a SEP IRA or a SIMPLE IRA. We refer to these as small business IRAs. These types of employer sponsored retirement plans offer a lot of benefits. They can allow for substantially higher contributions than regular IRAs. For instance, SEP IRAs can allow a small business owner to contribute up to 25% of the net earnings of the business or ,000 whichever is less. They are very cost effective when compared to other types of employer sponsored retirement plans because they don’t require you to manage and maintain a plan document or do certain IRS filings. However, they can have some downsides as well. For instance, SEP IRAs may require you to also make large contributions to employees. Additionally, small business IRAs don’t allow for Roth contributions and loans.</p>
                        <p>With all the little rules surrounding these types of plans, how do you know which one of any is best for you? Call us. We have been helping businesses like yours for years. After a close look at your situation, we can help you make the right decision and avoid pitfalls that could result in serious tax consequences or missed opportunities.</p>
                        <p>We provide the following services for these types of plans:</p>
                        
                        <ul>
                            <li>Plan Design and Compliance Assistance</li>
                            <li>Investment Management</li>
                            <li>Coordination with your Tax Professional</li>
                            <li>Participant Education and Communication</li>
                            <li>Regular Plan Sponsor and Participant Meetings</li>
                        </ul>
                    </div>

<?php include 'includes/servicesNav.php' ?>
                    <div class="extender"></div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(function () {
                $("#main-img").cycle({
                    fx: 'fade',
                    timeout: 10000
                });
            });
        </script>


<?php include 'includes/footer.php' ?>

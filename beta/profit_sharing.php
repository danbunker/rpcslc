<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

        <div id="corpus">
            <img src="images/inside_page_header.jpg" alt="banner" />
            <div id="content">
                <div class="tanBox" style="padding: 0;">
                    <div class="transBox fr editable" id="serviceBox">
                        <h5>Profit Sharing Plans</h5>
                        
                        <p>Our mission is to “increase the retirement readiness of employees and employers of small businesses”! Profit Sharing plans are one of the tools we use to accomplish that goal.</p>
                        <p>There are a lot of great benefits to offering a profit sharing plan. First, it can give employees a feeling that they are participating in the success of the business. Second, they can allow owners to make significant contributions, often as much as ,000, to their accounts. New comparability profit sharing plans can even allow business owners to target certain individuals with higher rates of contributions than the rest of the employee base. However, like all small business retirement plans, profit sharing plans can backfire if not designed with your specific goals in mind.</p>
                        <p>Profit sharing plans often include a 401(k) feature allowing participants to defer some of their own pay into the plan. Even when they are stand alone profit sharing plans, they often require many of the same compliance and participant education services.</p>
                        <p>We offer the following services to sponsors of profit sharing plans:</p>
                        
                        <ul>
                            <li>Plan Design and Compliance Assistance</li>    
                            <li>Customized Participant Education Plan; including regular live and web based meetings</li>    
                            <li>Proprietary Customized Digital Educational Material</li>    
                            <li>Investment Due Diligence</li>    
                            <li>Plan Sponsor Binder</li>    
                            <li>Plan Benchmarking</li>
                            <li>Regular Plan Sponsor Meetings</li>
                            <li>Retirement Readiness Score Card</li>
                            <li>Plan Administrator Liaison Services</li>
                            <li>Disclosure Checklist</li>
                        </ul>
                    </div>
<?php include 'includes/servicesNav.php' ?>
                    <div class="extender"></div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(function () {
                $("#main-img").cycle({
                    fx: 'fade',
                    timeout: 10000
                });
            });
        </script>


<?php include 'includes/footer.php' ?>

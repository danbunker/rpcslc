<?php
	include './common/header.php'
?>

		<div id="content">
			<div="first-block">
				<div id="inside-img">
					<img class="img-margin-top" src="./images/inside_page_header.jpg"/>
				</div>
			</div>
			<div id="second-block" style="height: 800px;">
				<div id="inside-full">
					<p class="inside-header">
						Our Services
					</p>
					<p class="inside-text">
						Although small business have many things in common, we have found that every small business has its
						own unique set of goals and objectives when it comes to a retirement plan. Some small businesses are
						primarily interested in recruiting and retaining employees while others are focused on owner contributions.
						What makes your company unique? How can we design a plan to meet your goals? How can we
						implement and monitor the plan so as to reduce your fiduciary liabilities and increase the participation and
						education of your employees? These are the questions that drive us. Below are the systems that we use to
						provide the solutions.
					</p>
					<p class="inside-header">
						Participant Services
					</p>
					<p class="inside-text">
						At RPC, we believe that the success or failure of your plan lies with the success or failure of each
						participant. That is why we pride ourselves on being the very best at employee education and participant
						services. Unlike many reps, we will be there for your employees. We have built a business on it. You can
						expect the following from RPC:
					</p>
					<p class="inside-text">
						<ul class="inside-text" style="margin-left: 25px;">
							<li>
								Personal, Bi-Lingual availability of our staff to yours
							</li>
							<li>
								On site group and individual enrollment meetings
							</li>
							<li>
								Online on-demand enrollment meetings at Discover401k.com
							</li>
							<li>
								Award Winning* online company specific education at Discover401k.com
							</li>
							<li>
								Online Spanish company specific education at Discover401k.com
							</li>
							<li>
								Pre enrollment announcements, posters, paycheck stuffers and much more
							</li>
							<li>
								Individual financial planning
							</li>
						</ul>
					</p>
					<p class="inside-header">
						Owner Services
					</p>
					<p class="inside-text">
						Small business retirement plans, when designed and maintained properly can provide significant benefits
						to the employer. The largest of these benefits are tax breaks & employee retention. Along with these
						benefits come responsibility and potential personal liability. Our owner services systems are designed to
						maximize plan sponsor benefits through proper plan design and minimize sponsor liability through ongoing
						monitoring and reporting. You can expect the following from RPC:
					</p>
					<p class="inside-text">
						<ul class="inside-text" style="margin-left: 25px;">
							<li>
								Regular Review of Plan Assets
							</li>
							<li>
								Regular Review of Investment Performance
							</li>
							<li>
								Investment Policy Statement Comparison
							</li>
							<li>
								Regular Review of Hot Audit Items
								<ul class="inside-text" style="margin-left: 25px;">
									<li>
										SPD Availability
									</li>
									<li>
										Contribution Timing
									</li>
									<li>
										Adequate Education
									</li>
								</ul>
							</li>
							<li>
								Regular Review of Participation Rates
							</li>
							<li>
								Review of Participant Education
							</li>
							<li>
								Maintenance of a Trustee file
							</li>
						</ul>
					</p>
					
					
					
					
				</div>
			</div>
<?php
	include './common/footer.php'
?>

		<div id="footer">

			<p>This material is for informational purposes only. Retirement Plan Consultants, Inc does not provide investment, tax or legal advice. It is your responsibility to select and monitor your investment options to meet your retirement objectives. You may also want to consult your own independent advisor as to any investment, tax or legal statements made herein.</p>

			<p>Securities and advisory services offered through LPL Financial, a registered investment advisor, <a href="http://www.finra.org/" target="_blank">FINRA</a>/<a href="http://www.sipc.org/" target="_blank">SIPC</a>. Retirement Plan Consultants, Inc is a separate entity from LPL Financial. Please see Disclosures for more details.</p>

			<p>NOT FDIC INSURED | MAY LOSE VALUE | NOT BANK GUARANTEED | NOT INSURED BY ANY GOVERNMENT AGENCY</p>

			<p>This site is designed for U.S. residents only. The services offered within this site are available exclusively through our U.S. Investment Representatives. The LPL Financial Registered Representatives associated with this site may only discuss and/or transact securities business with residents of the following states: AK, AL, AZ, CA, CO, FL, IA, ID, MA, MD, NV, OR, TX, UT, WA</p>

			<p>Fiduciary services are offered in advisory relationships only.</p>
            <p>Award winning education Best Life Skills Program of the Year. Awarded by ComputED Learning Centers, San Diego, the Education Software Review, 2006.</p>
			
			
		</div>
		
		<form name="form1" id="contactForm" action="" class="hidden">
            <dl>
                <dd>
                    <span class="closeForm">X</span>
                </dd>
                <dd style="margin: -35px 0 0 0;">
                    <label>First name:</label><br />
                    <input type="text" id="fname" autofocus required name="fname" placeholder="Enter your name"/>
                </dd>
                <dd>
                    <label>Last name:</label><br />
                    <input type="text"  id="lname" required name="lname" placeholder="Enter your last name"/>
                </dd>
                <dd>
                    <label>Email:</label><br />
                    <input type="email" name="email" id="email" required placeholder="Enter a valid email address"/>
                </dd>
                <dd class="companyName">
                    <label>Company:</label><br />
                    <input type="text" name="company"  id="company" placeholder="Enter your Company name"/>
                </dd>
                <dd class="formMessage">
                    <label>Message:</label><br />
                    <textarea   type="textarea" name="message" id="message" /></textarea>
                </dd>
                <dd style="text-align: center;">
                  <input type="hidden" id="fromType" name="formType">
                  <input type="submit" class="button" id="submit_btn" value="Send">
                </dd>
            </dl>
        </form>
	</div>
	
<script>
    $(".closeForm").click(function(){
      $('#contactForm').removeClass('shown');
      $('#contactForm').removeClass('newsLetter');
      $('#contactForm').removeClass('webminarForm');
      $('#contactForm').removeClass('help');
      $("#contactForm").addClass("hidden");
    });

    $(".nl").click(function(){
      $('#contactForm').removeClass('hidden');
      $('#contactForm').removeClass('newsLetter');
      $('#contactForm').removeClass('webminarForm');
      $('#contactForm').removeClass('help');
      $("#contactForm").addClass("shown newsLetter");
      $('#fromType').attr("value","nl");
    });

    $(".wb").click(function(){
      $('#contactForm').removeClass('hidden');
      $('#contactForm').removeClass('newsLetter');
      $('#contactForm').removeClass('webminarForm');
      $('#contactForm').removeClass('help');
      $("#contactForm").addClass("shown webminarForm");
      $('#fromType').attr("value","wb");
    });

    $(".hp").click(function(){
      $('#contactForm').removeClass('hidden');
      $('#contactForm').removeClass('newsLetter');
      $('#contactForm').removeClass('webminar');
      $('#contactForm').removeClass('help');
      $("#contactForm").addClass("shown help");
      $('#fromType').attr("value","hp");
    });


$(".button").click(function() {
    var fname = $("input#fname").val();
    var lname = $("input#lname").val();
    var email = $("input#email").val();
    var company = $("input#company").val();
    var message = $("input#message").val();

    var dataString = 'fname='+ fname + '&lname=' + lname + '&email=' + email + '&company=' + company + '&message=' + message;
            alert('Success');
    
    $.ajax({
        type: "GET",
        url: "emailProcessor.php",
        data: dataString,
        success: function(){
            alert('Success');
        }
    });

});

</script>

	
</body>
</html>
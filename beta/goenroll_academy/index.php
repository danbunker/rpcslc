<?php
	include './header.php'
?>

		<div id="content">
			<div="first-block">
			</div>
			<div id="second-block">
				<div id="main-left">
					<h3 style="padding: 10px;">
						Thank you for your interest in the Academy Mortgage Supplemental Life and Long Term Disability plans.  Open enrollment begins on March 12th and will go through March 25th.  Remember that the guarantee issue period for the supplemental Life will end on the 25th of March.  So if you want some term insurance up to the guarantee issue amount and you don’t want to hassle with health exams…Don’t miss open enrollment! 
						<br/><br/>
						Come back to this page and get enrolled beginning March 12th.
					</h3>
				</div>
				<div id="main-right" class="img-margin-top">
					<img src="/images/hartford_bi_alt_color_pos_jpg.JPG" border="0" width="250" style="margin: 15px;"/>
				</div>
			</div>
<?php
	include './footer.php'
?>

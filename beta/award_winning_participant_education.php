<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

        <div id="corpus">
            <img src="images/inside_page_header.jpg" alt="banner" />
            <div id="content">
                <div class="tanBox" style="padding: 0;">
                    <div class="transBox fr editable" id="serviceBox">
                        <h5>Award Winning Participant Education</h5>
                        
                        <p>Our mission is to “increase the retirement readiness of employees and employers of small businesses”!   What could be more at the core of this mission than participant education?  Retirement Plan Consultants has spent considerable time and money developing an award winning proprietary education platform.  You can learn more by visiting the 401(k) Education Center on this website.</p>
                        <p>While many advisors may consider participant education someone else’s responsibility, we have built our business around it.  The growth of 401(k) plans and subsequent replacement of other forms of retirement savings, like pensions, has put the burden of investment choice, savings rate choice, and distribution choice on the shoulders of participants.  In order to make good decisions in these areas, they need to have a better basic understanding of financial matters, especially retirement saving through 401(k) plans.</p>
                        <p>We will help you design and implement an education plan that can include live meetings, web meetings, recorded online classes, paycheck stuffers, home mailing campaigns and much more.  When it comes to participant education, Retirement Plan Consultants has the experience, knowledge and resources to help you make an impact.</p>
                    </div>
<?php include 'includes/servicesNav.php' ?>
                    <div class="extender"></div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(function () {
                $("#main-img").cycle({
                    fx: 'fade',
                    timeout: 10000
                });
            });
        </script>


<?php include 'includes/footer.php' ?>

<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

		<div id="corpus">
			<img src="images/inside_page_header.jpg" alt="banner" />
			<div id="content">
				<div class="bio single editable">
                    <img src="images/bio/Jere-Pic-xl.png" alt="Jere El-Bakri" class="fl" />
                    <p class="name">Jere El-Bakri, AIF, ERPA, QPA</p>
                    <p class="title">President-Retirement Plan Consultants, Inc.</p>

                    <ul>
                        <li>Registered Principal LPL Financial</li>
                        <li>AIF - Accredited Investment Fiduciary®</li>
                        <li>ERPA – Enrolled Retirement Plan Agent -IRS-</li>
                        <li>QPA – Qualified Plan Administrator -ASPA-</li>
                        <li>AAMS – Accredited Asset Management Specialist - College of Financial Planning</li>
                        <li>CRPS – Chartered Retirement Plan Specialist - College of Financial Planning</li>
                        <li>FINRA Series 7, 66, 24</li>
                        <li>Insurance License</li>
                    </ul>
                    <p>Jere El-Bakri is a Registered Principal of LPL Financial and the President of Retirement Plan Consultants, Inc. He graduated Magna Cum Laude with a BA in Microbiology from Utah State University. His career focus is to increase the retirement readiness of individuals, and to educate companies and their employees about retirement plans.</p>
                    <h5>Education and Designations</h5>
                    <p>At Retirement Plan Consultants, we believe in modern and dynamic education.  This is especially important considering many “best practice” and rule changes occur in the qualified plan arena each year.  Jere earned the Accredited Asset Management Specialist (AAMS) and Chartered Retirement Plan Specialist (CRPS) designations from the College for Financial Planning.  Additionally, Jere earned the Enrolled Retirement Plan Agent (ERPA) from the IRS and Qualified Pension Administrator (QPA) designation from the America Society of Pension Professionals and Actuaries.  These two designations help Jere serve clients in areas many advisors avoid, including plan design and compliance.   In 2012, Jere completed the Accredited Investment Fiduciary (AIF) program through FI 360.  This designation along with the education received and commitment to the AIF ethical policy has prepared Jere to assist plan sponsors with the selection and ongoing monitoring of the investment offerings in their plans.   In many cases, Jere can serve as a 3(21) fiduciary to the plan.</p>
                    <h5>Experience</h5>
                    <p>Jere has been securities licensed since June of 1998.  He founded Retirement Plan Consultants, Inc. in January of 2003 and began to focus on serving qualified plans, like 401(k) plans.  Because Jere believes that the success of the participant is central to the success of the plan, Jere has provided many (estimated over 700) live and web based education meetings since inception of Retirement Plan Consultants, Inc. in 2003.  The experience gained in front of everyday participants has been invaluable.  Additionally, Jere has significant experience meeting with investment committees and plan sponsors guiding them on their fiduciary responsibilities.  Finally, Jere has helped plan sponsors design and tailor their plans (estimated over 300 plan designs) to the needs of the individual company and participants within the plan.   This means that plan sponsors don’t have to just rely on the plan design ideas of compliance administrators, which are often not completely familiar with the business and the employees.</p>
                    <h5>Creativity</h5>
                    <p>Jere has helped create many proprietary education tools.  These tools can be found on this website by visiting the 401(k) Education Center.  Participants have become familiar with Jere’s voice as he often writes, produces, and narrates these classes.  Jere loves to create individualized education programs targeted specifically to the needs of each client.  His ability to create and publish web-based professionally recorded education classes provides a significant competitive advantage when considered by plan sponsors that are really interested in their participant’s retirement readiness.</p>
                    <p class="readMore"><a href="mailto:jere@rpcslc.com">Contact Jere...</a></p>


				</div>
			</div>
			<div class="extender"></div>
		</div>


<?php include 'includes/footer.php' ?>

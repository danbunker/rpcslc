<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

        <div id="corpus">
            <img src="images/inside_page_header.jpg" alt="banner" />
            <div id="content">
                <div class="tanBox" style="padding: 0;">
                    <div class="transBox fr editable" id="serviceBox">
                        <h5>401(k) Plans</h5>
                        <p>Our mission is to "increase the retirement readiness of employees and employers of small businesses"! 401(k) plans are one of the tools we use to accomplish that goal.</p>
                        <p>401(k) Plans are the most common form of small business retirement plans and for good reason. They offer flexibility, opportunity for employee and employer contributions, Roth and traditional contributions, loans and other great features. However, 401(k) plans also have issues to deal with. Sponsors often struggle with compliance, plan document maintenance, year-end testing, investment selection and monitoring, disclosures, participant education, expenses, fiduciary liability and rapidly changing governmental requirements. That is where we come in.</p>
                        <p>We are focused on small business qualified plans. We can make your plan work for you instead of the other way around. The following are some of the services we offer to plan sponsors:</p>
                        
                        <ul>
                            <li>Plan Design and Compliance Assistance</li>
                            <li>Customized Participant Education Plan; including regular live and web based meetings</li>
                            <li>Proprietary Customized Digital Educational Materials</li>
                            <li>Investment Due Diligence</li>
                            <li>Plan Sponsor Binder</li>
                            <li>Plan Benchmarking</li>
                            <li>Regular Plan Sponsor Meetings</li>
                            <li>Retirement Readiness Score Card</li>
                            <li>Plan Administrator Liaison Services</li>
                            <li>Disclosure Checklist</li>
                        </ul>
                    </div>
<?php include 'includes/servicesNav.php' ?>
                    <div class="extender"></div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(function () {
                $("#main-img").cycle({
                    fx: 'fade',
                    timeout: 10000
                });
            });
        </script>


<?php include 'includes/footer.php' ?>

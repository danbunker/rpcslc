<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

		<div id="corpus">
			<div id="content">

				<div id="main-img" class="fl editable">
					<img class="img-margin-top" src="images/banners/401K-Education-Banner.png"/>
				</div>
				
				<div class="tanBox editable" style="margin: 0 0 0 685px; height: 460px;">
				    <h5 style="color: #A54C11;">Stay Informed RPC Newsletter</h5>
				    <p>Join our monthly newsletter to receive all the latest news on planning your financial future.</p>
				    <p><a href="#" class="nl"><img src="images/NewsLetter-Join.png" alt="Join" /></a></p> 
                    <p class="fr"><a href="newsletters/May_2013_Newsletter.pdf" target="_blank" id="joinNewsletter"><img src="images/Newsletter-PDF-Download.png" alt="Join" /></a></p> 
                    <h5>Current Newsletter PDF Download</h5>
				</div>
				
				<div class="tanBox fl" style="width: 655px; margin-top: -300px;">
					<div class="editable">
						<img src="images/WebinarSeries-Icon.png" style="width: 85px" alt="Webminar Series" class="fl" />
						<h5 style="color: #A54C11;margin-left: 100px;" >Retirement Education Webinar Series</h5>
                        <p class="fr"><a href="#" class="wb"><img src="images/Zone4-Register.png" alt="Click Here" style="width: 80px; position: relative; right: 40px;" /></a></p>
						<h5 style="margin-left: 100px;">Next Webinar "Fundamentals of 401(<span style="text-transform: lowercase;">k</span>) Plans"</h5>
						<p style="margin-left: 100px;">scheduled for June 11, 2013 at 7pm</p>
					</div>
                    <div class="webminar editable" style="margin-top: 20px;">
                        <h5 style="color: #A54C11;">What would you like to learn about today?</h5>
                        <p>Choose your topic . . .</p>
                        <dl>
                        	<dd>
                        	    <label>Fundamentals of<strong>401(<span style="text-transform: lowercase;">k</span>) Plans</strong></label>
                        	    <a href="http://united401k.adobeconnect.com/basic401k" rel="shadowbox[series]"><span>Click Here Webminar</span></a>
                    	    </dd>
                    	    <dd>
                                <label><strong>401(<span style="text-transform: lowercase;">k</span>)</strong>Investment Concepts</label>
                                <a href="http://united401k.adobeconnect.com/invest" rel="shadowbox[series]"><span>Click Here Webminar</span></a>
                            </dd>
                            <dd>
                                <label><strong>Retirement</strong>Income Planning</label>
                                <a href="http://united401k.adobeconnect.com/retireready" rel="shadowbox[series]"><span>Click Here Webminar</span></a>
                            </dd>
                            <dd>
                                <label>Enrolling in a<strong>401(<span style="text-transform: lowercase;">k</span>)</strong></label>
                                <a href="videos/401k/index.html"  rel="shadowbox;width=800;height=600"><span>Click Here Webminar</span></a>
                            </dd>
                            <dd>
                                <label>Need some individual<strong>help?</strong></label>
                                <a href="#" class="hp"><span>Click Here Webminar</span></a>
                            </dd>
                        </dl>
                        <div class="extender"></div>
                    </div>
				</div>

			</div>
				<div class="extender"></div>
		</div>

		<script type="text/javascript">
			$(function () {
				$("#main-img").cycle({
					fx: 'fade',
					timeout: 10000
				});
			});
		</script>


<?php include 'includes/footer.php' ?>

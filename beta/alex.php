<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

		<div id="corpus">
			<img src="images/inside_page_header.jpg" alt="banner" />
			<div id="content">
				<div class="bio single editable">
                    <img src="images/bio/Alex-Pic-xl.png" alt="Alex Helmuth" class="fl" />
                    <p class="name">Alex Helmuth</p>
                    <p class="title">LPL Financial Planner</p>

                    <ul>
                        <li>LPL Registered Representative</li>
                        <li>FINRA Series 7</li>
                        <li>FINRA Series 66</li>
                    </ul>
                    
                    <p>Alex Helmuth is an LPL Financial Planner at Retirement Plan Consultants.  Alex helps to educate 401(k) participants about all investment vehicles available in preparing for retirement.  His focus is to help increase retirement readiness by working closely with families and individuals to create in depth financial and retirement income plans by utilizing potential investments opportunities both within 401(k) plans, and external individual wealth building investment accounts. </p>
                    <p>Alex graduated from the University of Iowa with a BBA in Finance and a Minor in Economics. His time and course load at the University leaned heavily toward individual financial planning. After graduation, he spent the first two years of his career working in the fast paced financial services industry in Iowa.  With a desire to move west and work with Jere El-Bakri in the retirement planning field, Alex joined Retirement Plan Consultants in July 2012, </p>
                    <p>Alex Helmuth is a Registered Representative of LPL Financial.  He holds a FINRA Series 7 and Series 66 registrations through LPL Financial. Alex compliments Retirement Plan Consultants, Inc. by providing resources and investment advice to individuals looking for individual financial planning and wealth management services.  </p>

                    <p class="readMore"><a href="mailto:alex@rpcslc.com">Contact Alex...</a></p>
				</div>
			</div>
			<div class="extender"></div>
		</div>


<?php include 'includes/footer.php' ?>

<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

        <div id="corpus">
            <img src="images/inside_page_header.jpg" alt="banner" />
            <div id="content">
                <div class="tanBox" style="padding: 0;">
                    <div class="transBox fr editable" id="serviceBox">

                        <h5>Individual Retirement Planning</h5>
                        
                        <p>Our mission is to “increase the retirement readiness of employees and employers of small businesses”! Creating a detailed retirement road map is an essential step in the retirement readiness process.</p>
                        <p>In the end, the big question is “will I have enough money saved to give me a paycheck for life?” Unfortunately, this easy question is not easily answered. For instance, people often start retirement planning by trying to figure out how much income a retirement portfolio would have to create to last their entire life. That question leads to other questions. How much will inflation affect my retirement need? What will I get from other sources like social security or pensions? How much should I expect to pay in taxes? It takes answers to these and other questions just to figure out how much income you need, let alone figuring out how much you have to save to generate that income.</p>
                        <p>Then you have to ask yourself the “what if” questions. What if my spouse or I passed away earlier than expected? What if one of us became disabled or had to enter a long term care facility? What if the investment markets don’t recover? What if inflation spikes? Etc. Etc.</p>
                        <p>Individual Retirement Planning is the ongoing process of answering these questions and then using those answers to take action steps that make a difference. Our planning tools range from basic 401(k) income calculators that we often use onsite at 401(k) enrollment meetings, to in depth planning and monitoring tools. We look forward to helping you find your answers.</p>

                    </div>
<?php include 'includes/servicesNav.php' ?>
                    <div class="extender"></div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(function () {
                $("#main-img").cycle({
                    fx: 'fade',
                    timeout: 10000
                });
            });
        </script>


<?php include 'includes/footer.php' ?>

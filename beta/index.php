<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

		<div id="corpus">
			<div id="content">

				<div id="main-img" class="fl editable">
					<a href="plan_design_services.php"><img class="img-margin-top" src="images/banners/Banner1-PlanDesign.png"/></a>
					<a href="fiduciary_services.php"><img class="img-margin-top" src="images/banners/Banner2-FiduciaryServices.png"/></a>
					<a href="401k_education_center.php"><img class="img-margin-top" src="images/banners/Banner3-ParticipantEdu.png"/></a>
					<img class="img-margin-top" src="images/banners/Banner4-AllServices.png"/>
				</div>
				
				<div class="grBox" style="margin: 0 0 0 685px;">
					<h5>401(k) Participant Education</h5>
					<p>We created an award winning interactive employee education course to help plan sponsors fulfill their responsibility to educate employees.</p>
					<p>Give it a try (Don’t forget to turn on your speakers).</p>
					<p>
						<a href="videos/401k/index.html"  rel="shadowbox;width=800;height=600"><img class="img-margin-top video-img" src="./images/main-video.gif" border="0"/></a>
					</p>
					<img src="images/401KEducation-Icon.png" alt="education" class="fl" style="width: 40px; margin: -10px 0 0 0;" />
					<h5 style="font-size: 0.97em;"><em>Visit Our</em></h5>
					<h4 style="font-size: 0.97em;"><a class="fr" href="401k_education_center.php"><img src="images/401KEducation-Enter.png" alt="enter"  style="width: 80px; margin-top: -10px" /></a>401(k) Education Center </h4>
				</div>
				
				<div class="tanBox fl" style="width: 655px; margin-top: -55px;">
					<div class="fl editable" style="padding: 10px; border-right: #666 dotted 1px; width: 50%;">
						<h5>Retirement Readiness Services</h5>
						<ul>
							<li><a href="401k_plans.php">» 401(k) Plans</a></li>
							<li><a href="individual_retirement_planning.php">» Individual Retirement Planning</a></li>
							<li><a href="plan_design_services.php">» Plan Design Services</a></li>
							<li><a href="fiduciary_services.php">» Fiduciary Services</a></li>
							<li><a href="award_winning_participant_education.php">» Award Winning Participant Education</a></li>
						</ul>
					</div>
					<div style="margin: 0 0 0 360px;" class="editable">
					  <h5>Retirement Education Webinar Series</h5>
					  <h5 class="subheader">Next Webinar- "JULY 11"</h5>
					  <p class="orange">401(k) Investment Concepts</p>
					  <img src="images/WebinarSeries-Icon.png" alt="webminar services" style="width: 110px; margin-top: 25px; margin-left: 30px;" />
					  <a href="401k_education_center.php" style="display: block; float: right; margin-top: 60px; margin-right: 50px;"><img src="images/Banners_1-2-3-LearnMore.png" alt="Learn more" style="width: 90px;" /></a>
					</div>
				</div>


				<div class="transBox" style="margin: 0 0 0 685px;">
					<a href="#" id="joinNewsletter"><img src="images/NewsLetter-Join.png" alt="Join Newsletter" class="fr nl" /></a>
					<h5 style="font-size: 15px;">RPC Monthly Newsletter</h5>
				</div>
				

			</div>
				<div class="extender"></div>
		</div>

		<script type="text/javascript">
			$(function () {
				$("#main-img").cycle({
					fx: 'fade',
					timeout: 10000
				});
			});
		</script>


<?php include 'includes/footer.php' ?>

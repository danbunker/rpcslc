<?php

$newsletter = "website.rpcslc@gmail.com";
$webinar = "website.rpcslc@gmail.com";
$help = "website.rpcslc@gmail.com";
$freeClassRequest = "website.rpcslc@gmail.com";

$from = "website@rpcslc.com";


$firstName=$_GET['fname'];
$lastName=$_GET['lname'];
$email=$_GET['email'];
$company=$_GET['company'];
$message=$_GET['message'];
$sendInfomration=$_GET['sendInfomration'];
$formType=$_GET['formType'];

if($formType=='nl'){
    $to = $newsletter;
    $subject = "Website Newsletter Signup";
    $message = "Name: $firstName $lastName \n Email: $email";
    $headers = "From:" . $from;
    // mail($to,$subject,$message,$headers);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, 'api:key-04601611b1f0e3445279091dfbd4260a');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_URL,
                'https://api.mailgun.net/v3/mg.rpcslc.com/messages');
    curl_setopt($ch, CURLOPT_POSTFIELDS,
                  array('from' => $from,
                        'to' => $to,
                        'subject' => $subject,
                        'text' => $message));
    $result = curl_exec($ch);
    curl_close($ch);
    header("location:emailSuccess.php?id=nl");
}

if($formType=='wb'){
    $to = $webinar;
    $subject = "Website Webminar Signup";
    $message = "Name: $firstName $lastName \n Company: $company \n Email: $email";
    $headers = "From:" . $from;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, 'api:key-04601611b1f0e3445279091dfbd4260a');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_URL,
                'https://api.mailgun.net/v3/mg.rpcslc.com/messages');
    curl_setopt($ch, CURLOPT_POSTFIELDS,
                  array('from' => $from,
                        'to' => $to,
                        'subject' => $subject,
                        'text' => $message));
    $result = curl_exec($ch);
    curl_close($ch);
    // mail($to,$subject,$message,$headers);
    header("location:emailSuccess.php?id=wb");
}

if($formType=='hp'){
    $to = $help;
    $subject = "Website Help Request";
    $message = "Name: $firstName $lastName \n Company: $company \n Email: $email \n Message: $message";
    $headers = "From:" . $from;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, 'api:key-04601611b1f0e3445279091dfbd4260a');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_URL,
                'https://api.mailgun.net/v3/mg.rpcslc.com/messages');
    curl_setopt($ch, CURLOPT_POSTFIELDS,
                  array('from' => $from,
                        'to' => $to,
                        'subject' => $subject,
                        'text' => $message));
    $result = curl_exec($ch);
    curl_close($ch);
    // mail($to,$subject,$message,$headers);
    header("location:emailSuccess.php?id=hp");
}

if($formType=='fc'){
    $to = $freeClassRequest;
    $subject = "Free Class Request Request";
    $message = "Name: $firstName $lastName \n Company: $company \n Email: $email \n $sendInfomration";
    $headers = "From:" . $from;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, 'api:key-04601611b1f0e3445279091dfbd4260a');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_URL,
                'https://api.mailgun.net/v3/mg.rpcslc.com/messages');
    curl_setopt($ch, CURLOPT_POSTFIELDS,
                  array('from' => $from,
                        'to' => $to,
                        'subject' => $subject,
                        'text' => $message));
    $result = curl_exec($ch);
    curl_close($ch);
    // mail($to,$subject,$message,$headers);
    header("location:emailSuccess.php?id=fc");
}
?>

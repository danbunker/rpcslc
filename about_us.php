<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

		<div id="corpus">
			<img src="images/inside_page_header.jpg" alt="banner" />
			<div id="content">
				<div class="bio editable" id="untitled-region-1"><h5>Our Company</h5>

<p>Our mission is to "increase the retirement readiness of employees and
employers of small businesses"! Small business IRAs are one of the tools we use
to pursue that goal. Retirement Plan Consultants, Inc (RPC) specializes in
Small Business Qualified Plans such as Simple IRAs, SEP IRAs, 401(k) Plans,
Standard, Age Weighted and Cross Tested Profit Sharing Plans. Our partners
include some of the most recognized third party administration firms and
investment platforms in the country. We are focused on assisting with the
design, implementation and maintenance of your plan, so that it is the best
plan possible for your business.</p>

<p>It has been said that brakes don't stop your car, tires do. We believe that
success at the participant level is where the rubber meets the road with a
small business retirement plan.</p>

<h5>Our Leadership</h5>

<p><img alt="Jere El-Bakri" class="fl" src="images/bio/Jere.png" /></p>

<p class="name">Jere El-Bakri, AIF, ERPA, QPA</p>

<p class="title">President-Retirement Plan Consultants, Inc.</p>

<p>Jere El-Bakri is a Registered Principal of LPL Financial and the President
of Retirement</p>

<p>Plan Consultants, Inc. He graduated Magna Cum Laude with a BA in
Microbiology from Utah State University. His career focus is to increase the
retirement readiness of individuals, and to educate companies and their
employees about retirement plans.</p>

<p class="readMore"><a href="jere.php">Read More...</a></p>

<p><img alt="Jennifer Simonich" class="fl" src="images/bio/Jennifer.png" /></p>

<p class="name">Jennifer Simonich, CRPS</p>

<p class="title">Director of Client Services – Retirement Plan Consultants,
Inc.</p>

<p>Jennifer Simonich is the Director of Client Services at Retirement Plan
Consultants, Inc. She shares in Retirement Plan Consultants' vision to increase
the retirement readiness of employers and employees. Jennifer and her team work
to systematically educate and provide service to our clients and the
participants of 401(k) plans that we serve.</p>

<p class="readMore"><a href="jennifer.php">Read More...</a></p>

<p><img alt="Alex Helmuth" class="fl" src="images/bio/Alex.png" /></p>

<p class="name">Alex Helmuth</p>

<p class="title">LPL Financial Planner</p>

<p>Alex Helmuth is an LPL Financial Planner at Retirement Plan Consultants.
Alex helps to educate 401(k) participants about all investment vehicles
available in preparing for retirement. His focus is to help increase retirement
readiness by working closely with families and individuals to create in depth
financial and retirement income plans by utilizing potential investments
opportunities both within 401(k) plans, and external individual wealth building
investment accounts.</p>

<p class="readMore"><a href="alex.php">Read More...</a></p>

<p><img alt="Brooke Thomas" class="fl" src="images/bio/Brooke.png" /></p>

<p class="name">Brooke Thomas</p>

<p class="title">Participant Services Coordinator</p>

<p>Brooke Thomas is the Participant Services Coordinator at Retirement Plan
Consultants. She provides ongoing service and education to 401(k) participants,
and shares in our goal to help increase the retirement readiness participants
that we serve. Whether participants have a question about how to get enrolled,
about their employer contribution, or questions about beneficiary changes or
withdrawal options, Brooke is there to help the participants along the way.</p>

<p class="readMore"><a href="brooke.php">Read More...</a></p></div>
			</div>
			<div class="extender"></div>
		</div>


<?php include 'includes/footer.php' ?>

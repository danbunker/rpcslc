<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

        <div id="corpus">
            <img src="images/inside_page_header.jpg" alt="banner" />
            <div id="content">
                <div class="tanBox" style="padding: 0;">
                    <div class="transBox fr editable" id="serviceBox">
                        <h5>Plan Design Assistance and Compliance Administration Liaison Services</h5>
                        <p>Our mission is to "increase the retirement readiness of employees and employers of small businesses"!  For that to occur, the plan just needs to run correctly and provide the best possible opportunities for all employees, including owners, to save as much as they can for retirement.  Retirement Plan Consultants, Inc. is in a very unique position from an education and experience perspective to help plan sponsors with plan design and plan administration.</p>
                        <p>If you have sponsored a plan for some time, you have undoubtedly run into things like compliance testing, data gathering for 5500 filings, file and contribution submission and many other day to day workings of the plan.  You may have wished you had professional assistance in these areas.  We have a staff of 5 with extensive administration experience and education.  Jere El-Bakri has received the QPA (Qualified Pension Administrator) designation from ASPPA (American Society of Pension Professionals and Actuaries).  Branch Operations Manager Jennifer Simonich has received the CRPS (Chartered Retirement Plan Specialist) designation from the College for Financial Planning, and has extensive experience in plan administration. Her experience also includes working for over 7 years in a Third Party Administration firm.</p>
                        <p>Plan design may be the single most important factor when it comes to helping employees and owners prepare for retirement.  Retirement Plan Consultants has helped hundreds of plan sponsors design a plan that is right for them.  You can learn more about plan design and even get an idea of what general design might be right for you by taking our free digital class <a href="http://united401k.adobeconnect.com/designtool">click here</a>.In the end, the plan needs to work and having an independent partner like Retirement Plan Consultants can be a great resource for a plan sponsor. </p>
                    </div>
<?php include 'includes/servicesNav.php' ?>
                    <div class="extender"></div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(function () {
                $("#main-img").cycle({
                    fx: 'fade',
                    timeout: 10000
                });
            });
        </script>


<?php include 'includes/footer.php' ?>

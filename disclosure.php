<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

		<div id="corpus">
			<img src="images/inside_page_header.jpg" alt="banner" />
			<div id="content">
				<div class="bio single editable">
				    
				    
                    <h5>Disclosure</h5>
                    <p>The information contained on this site is not intended for distribution to, or use by, any person or entity in any jurisdiction or country where such distribution would subject Retirement Plan Consultants, Inc (RPC), LPL Financial, or any affiliates, to any registration requirement within such jurisdiction or country. The information obtained on this site is intended for informational purposes only and does not constitute a solicitation or offer to initiate a transaction in any securities product or investment advisory service. Further, nothing on this site should be construed as rendering legal or tax advice.</p>
                    <p>Advice can only be rendered after a person has had personal contacts, completed the Retirement Plan Consultants, Inc planning processes, and upon the execution of all appropriate account forms. Retirement Plan Consultants, Inc will not effect or attempt to effect transactions in securities, or the rendering of personalized investment advice for compensation over the Internet.</p>
                    <p>At certain places on this Retirement Plan Consultants, Inc. Internet site, live 'links' to other Internet addresses can be accessed. Such external Internet addresses contain information created, published, maintained, or otherwise posted by institutions or organizations *independent of RPC and LPL Financial. Retirement Plan Consultants, Inc. and LPL Financial do not endorse, approve, certify, or control these external Internet addresses and does not guarantee or assume responsibility for the accuracy, completeness, efficacy, timeliness, or correct sequencing of information located at such addresses. Use of any information obtained from such addresses is voluntary, and reliance on it should only be undertaken after an independent review of its accuracy, completeness, efficacy, and timeliness. Reference therein to any specific commercial product, process, or service by trade name, trademark, service mark, manufacturer, or otherwise does not constitute or imply endorsement, recommendation, or favoring by Retirement Plan Consultants, Inc or LPL Financial.</p>
                    <p>By accessing this website and the materials and information contained herein, you acknowledge that you have read, understand, accept and agree to be bound by the following terms and conditions:</p>
                    
                    <h5>No Warranty</h5>
                    <p>While RPC strives to provide uninterrupted access to its websites, it is possible for arbitrary circumstances to prevent certain applications from being accessible. All users agree to hold RPC, LPL Financial, and any affiliates thereto, harmless against any claim of liability surrounding accessibility of this website, any application contained herein, or any application accessible through a link provided herein.</p>
                    
                    <h5>Secured Site Access</h5>
                    <p>Certain portions of this site may be restricted and require authorization to access. Unauthorized use of or access to these restricted areas is expressly prohibited and may result in criminal and/or civil prosecution. Attempts to access such areas without authorization may be viewed, monitored and recorded and any information obtained may be given to law enforcement organizations in connection with any investigation or prosecution of possible criminal activity on this system.</p>
                    
                    <h5>Online Information and Tools</h5>
                    <p>Certain portions of Retirement Plan Consultants, Inc (RPC) web site (i.e. newsletters, articles, commentaries, etc.) may contain a discussion of, and/or provide access to RPC’s(and those of other investment and non-investment professionals) positions and/or recommendations as of a specific prior date. Due to various factors, including changing market conditions, such discussion may no longer be reflective of current position(s) and/or recommendation(s). Moreover, no client or prospective client should assume that any such discussion serves as the receipt of, or a substitute for, personalized advice from RPC, or from any other investment professional. Retirement Plan Consultants, Inc is neither an attorney nor an accountant, and no portion of the web site content should be interpreted as legal, accounting or tax advice.</p>
                    <p>To the extent that any client or prospective client utilizes any economic calculator or similar device contained within or linked to RPC’s web site, the client and/or prospective client acknowledges and understands that the information resulting from the use of any such calculator/device, is not, and should not be construed, in any manner whatsoever, as the receipt of, or a substitute for, personalized individual advice from RPC or from any other investment professional.</p>
                    
                    <h5>Additional Items</h5>
                    <p>All articles, links or additional items are provided for informational purposes only and were not created by Retirement Plan Consultants, Inc. or LPL Financial. Before implementing any strategy discussed herein, you should consult with your own financial, tax, and/or legal advisors to determine its applicability in light of your own situation.</p>
                    <p>Neither Retirement Plan Consultants, Inc., LPL Financial nor their representatives or agents are permitted to give legal or tax advice. Any discussion of taxes is for general informational purposes only. Such discussion does not purport to be complete or to cover every situation. You should seek advice based on your particular circumstances from an independent tax advisor.</p>
                    <p>Most insurance policies and annuity contracts contain exclusions, limitations, reductions of benefits, surrender charges and terms for keeping them in force. Your representative can provide you with costs and complete details.</p>
                    
                    <h5>Downloaded Material From Website</h5>
                    <p>You may download or print out a hard copy of individual pages and/or sections of the RPC website, and files made available for download, provided that you do not remove any logos or other proprietary notices. Any downloading or otherwise copying from the RPC website will not transfer title to any software or material to you. You may not reproduce (in whole or in part), transmit (by electronic means or otherwise), modify, link into or use for any public or commercial purpose the RPC website without the prior written permission of RPC.</p>				    
                    <p>Although this website may provide general information about the potential impact of taxes on various investment scenarios and planning, it should be made clear that LPL Financial Consultants do not render either legal or tax advice. Please consult a legal or tax professional for any specific advice.</p>
                    <p>Business Continuity Plan for LPL Financial is available at <a href="http://lplfinancial.lpl.com/">http://lplfinancial.lpl.com/</a>.                    </p>



				</div>
			</div>
			<div class="extender"></div>
		</div>


<?php include 'includes/footer.php' ?>

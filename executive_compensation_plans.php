<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

        <div id="corpus">
            <img src="images/inside_page_header.jpg" alt="banner" />
            <div id="content">
                <div class="tanBox" style="padding: 0;">
                    <div class="transBox fr editable" id="serviceBox">
                        <h5>Executive Compensation Plans</h5>
                        
                        <p>Our mission is to “increase the retirement readiness of employees and employers of small businesses”! Executive Compensation plans are one of the tools we use to accomplish that goal.</p>
                        <p>Sometimes referred to as "Non Qualified Deferred Compensation Plans", executive compensation plans offer some great benefits. For instance, contribution limits can be set very high. They provide for tax deferred savings for the participants and can be designed very specifically to fit the needs and wants of the employer. However, there are also quite a few pit falls and rules to follow to maintain a compliant plan. Due to the extensive ability to customize and high degree of employee education required in these types of plans, you need to make sure you use a firm that has the resources and experience to help you get it right.</p>
                        <p>Executive compensation plans can give your company a retention tool and allow your highly paid employees to save enough to replace a larger portion of their income at retirement. A common design in these types of plans allows highly compensated employees that may be otherwise limited by your 401(k) plan to contribute the full qualified plan IRS limit. Additionally, you can individualize who is eligible and even have special vesting schedules.</p>
                        <p>Here are some services we provide to sponsors of executive compensation plans:</p>
                        
                        <ul>
                            <li>Plan Design and Compliance Assistance</li>    
                            <li>Customized Participant Education Plan; including regular live and web based meetings</li>    
                            <li>Proprietary Customized Digital Educational Materials</li>    
                            <li>Investment Due Diligence</li>    
                            <li>Regular Plan Sponsor Meetings</li>    
                            <li>Plan Administrator Liaison Services</li>    
                            <li>One on one Participant Meetings</li>    
                        </ul>
                    </div>
<?php include 'includes/servicesNav.php' ?>
                    <div class="extender"></div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(function () {
                $("#main-img").cycle({
                    fx: 'fade',
                    timeout: 10000
                });
            });
        </script>


<?php include 'includes/footer.php' ?>

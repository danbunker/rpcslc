<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

		<div id="corpus">
			<img src="images/inside_page_header.jpg" alt="banner" />
			<div id="content">
				<div class="bio single editable">
                    <img src="images/bio/Jennifer-Pic-xl.png" alt="Jennifer Simonich" class="fl" />
                    <p class="name">Jennifer Simonich, CRPS</p>
                    <p class="title">Director of Client Services – Retirement Plan Consultants, Inc.</p>
                    <ul>
                        <li>Registered LPL Branch Operations Manager</li>
                        <li>CRPS – Chartered Retirement Plan Specialist – College of Financial Planning</li>
                        <li>FINRA Series 7</li>
                        <li>FINRA Series 63</li>
                    </ul>

                    <p>Jennifer Simonich is the Director of Client Services at Retirement Plan Consultants, Inc. She shares in Retirement Plan Consultants’ vision to increase the retirement readiness of employers and employees.  Jennifer and her team work to systematically educate and provide service to our clients and the participants of 401(k) plans that we serve.</p>
                    <p>Jennifer joined Retirement Plan Consultants, Inc. in 2005, and has over 15 years of experience working in the financial services industry. Included in her 15 years within the financial services industry are over 7 years of experience in the third party administration of 401(k) plans. She is a Registered LPL Branch Operations Manager, and holds both a FINRA Series 7 and Series 63 registrations through LPL Financial.  </p>
                    <p>Jennifer has received the CRPS (Chartered Retirement Plan Specialist) designation from the College for Financial Planning. CRPS is a designation awarded to financial professionals who have successfully completed an independent course, pass a final examination, and sign a code of ethics commitment. CRPS graduates receive the foundation needed to properly navigate the complexities associated with establishing and servicing employer sponsored retirement plans.   Jennifer relies upon her 15 years of experience, and the knowledge and values associated with her CRPS designation in her client oriented approach to successfully educate and service plan sponsors and their participants.  </p>
                    <p>Whether you are a plan sponsor or a plan participant, Jennifer is always available and willing to answer any question you may have.</p>                 

                    <p class="readMore"><a href="mailto:jennifer@rpcslc.com">Contact Jennifer...</a></p>

				</div>
			</div>
			<div class="extender"></div>
		</div>


<?php include 'includes/footer.php' ?>

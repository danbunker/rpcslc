<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>



		<div id="corpus">
			<div id="content">

				<div id="main-img" class="fl editable"><p><a href="plan_design_services.php"> <img class="img-margin-top" src="images/banners/Banner1-PlanDesign.png" alt=""></a> <a href="fiduciary_services.php"> <img class="img-margin-top" src="images/banners/Banner2-FiduciaryServices.png" alt=""></a> <a href="401k_education_center.php"> <img class="img-margin-top" src="images/banners/Banner3-ParticipantEdu.png" alt=""></a> <img class="img-margin-top" src="images/banners/Banner4-AllServices.png" alt=""></p></div>

				<div class="grBox" style="margin: 0 0 0 685px;">
					<h5>401(k) Participant Education</h5>
					<p>We created an award winning interactive employee education course to help plan sponsors fulfill their responsibility to educate employees.</p>
					<p>Give it a try (Don’t forget to turn on your speakers).</p>
					<p>
						<a href="videos/401k/index.html"  rel="shadowbox;width=800;height=600"><img class="img-margin-top video-img" src="./images/main-video.gif" border="0"/></a>
					</p>
					<img src="images/401KEducation-Icon.png" alt="education" class="fl" style="width: 40px; margin: -10px 0 0 0;" />
					<h5 style="font-size: 0.97em;"><em>Visit Our</em></h5>
					<h4 style="font-size: 0.97em;"><a class="fr" href="401k_education_center.php"><img src="images/401KEducation-Enter.png" alt="enter"  style="width: 80px; margin-top: -10px" /></a>401(k) Education Center </h4>
				</div>

				<div class="tanBox fl" style="width: 655px; margin-top: -55px;">
					<div class="fl editable" style="padding: 10px; border-right: #666 dotted 1px; width: 50%;" id="untitled-region-1"><h5>Retirement Readiness Services</h5>
<ul>
<li><a href="401k_plans.php">» 401(k) Plans</a></li>
<li><a href="individual_retirement_planning.php">» Individual Retirement Planning</a></li>
<li><a href="plan_design_services.php">» Plan Design Services</a></li>
<li><a href="fiduciary_services.php">» Fiduciary Services</a></li>
<li><a href="award_winning_participant_education.php">» Award Winning Participant Education</a></li>
</ul></div>
			<div style="margin: 0 0 0 360px;" class="editable" id="untitled-region-2"><h5> </h5>
<h5>Retirement Education Webinar Series</h5>
<h5 class="subheader"> </h5>
<h5 class="subheader">Next Webinar- November 17, 2015, 11:00am MST</h5>
<p class="orange">"Fundamentals of 401(k) Plans"</p>
<p><img style="width: 116px; margin-top: 25px; margin-left: 30px;" src="images/Discover401k-Mouse-Pad.png" alt="webminar services" width="182" height="118"> <a class="wb" style="margin-top: 60px; margin-right: 50px; float: right; display: block;" href="#"><img style="width: 93px;" src="images/Plan-Review.png" alt="Learn more" width="123" height="50"></a> <!-- <a href="401k_education_center.php" style="display: block; float: right; margin-top: 60px; margin-right: 50px;"><img alt="Learn more" height="45" src="images/Free-Plan-Review.png" style="width: 93px;" width="123" /></a> --></p></div>
				</div>


				<div class="transBox editable" style="margin: 0 0 0 685px;" id="untitled-region-3"><table border="0">
<tbody>
<tr>
<td class="monthlyNewsletter">
<p><strong>RPC Monthly Newsletter</strong></p>
</td>
<td>
<h5><a id="joinNewsletter" href="#" name="joinNewsletter"><img class="fr nl" src="images/NewsLetter-Join.png" alt="Join Newsletter"></a></h5>
</td>
</tr>
<tr>
<td class="monthlyNewsletter"><strong>LPL Account View Log In</strong></td>
<td>
<h5> <a title="LPL My Account View" href="https://myaccountviewonline.com" target="_blank"><img src="Click-Here.png" alt="" width="79" height="28"></a></h5>
</td>
</tr>
<tr>
<td class="monthlyNewsletter"><strong>Plan Sponsor Document Vault</strong></td>
<td>  <a title="Plan Sponsor Documents" href="https://retirementplanconsultants.sharefile.com/" target="_blank"><img src="Click-Here.png" alt="" width="80" height="27"></a></td>
</tr>
<tr>
<td class="monthlyNewsletter"> </td>
<td> </td>
</tr>
<tr>
<td class="monthlyNewsletter"> </td>
<td> </td>
</tr>
<tr>
<td class="monthlyNewsletter"> </td>
<td> </td>
</tr>
</tbody>
</table></div>


			</div>
				<div class="extender"></div>
		</div>


		<script type="text/javascript">

			// required to create the fade image cycle on the main page.
			$("#main-img p").cycle({
				fx: 'fade',
				 timeout: 10000
			});
		</script>


<?php include 'includes/footer.php' ?>

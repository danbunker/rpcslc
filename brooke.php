<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

		<div id="corpus">
			<img src="images/inside_page_header.jpg" alt="banner" />
			<div id="content">
				<div class="bio single editable">
                    <img src="images/bio/Brooke-Pic-xl.png" alt="Brooke Thomas" class="fl" />
                    <p class="name">Brooke Thomas</p>
                    <p class="title">Participant Services Coordinator</p>

                    <p>Brooke Thomas is the Participant Services Coordinator at Retirement Plan Consultants.  She provides ongoing service and education to 401(k) participants, and shares in our goal to help increase the retirement readiness participants that we serve.  Whether participants have a question about how to get enrolled, about their employer contribution, or questions about beneficiary changes or withdrawal options, Brooke is there to help the participants along the way.</p>
                    <p>Brooke joined Retirement Plan Consultants in February 2013.  Prior to joining our firm, she had 4 years of experience working in the financial service and customer service arenas. Brooke leverages this experience to provide 401(k) participants with personalized and responsive service.  She is always motivated to expand her knowledge base and is currently studying for the Qualified 401(k) Administrator (QKA) designation from ASPPA.</p>

                    <p class="readMore"><a href="mailto:brooke@rpcslc.com">Contact Brooke...</a></p>

				</div>
			</div>
			<div class="extender"></div>
		</div>


<?php include 'includes/footer.php' ?>

<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

		<div id="corpus">
			<div id="content">

				<div id="main-img">
					<img class="img-margin-top" src="images/banners/Banner1-PlanDesign.png"/>
					<img class="img-margin-top" src="images/banners/Banner2-FiduciaryServices.png"/>
					<img class="img-margin-top" src="images/banners/Banner3-ParticipantEdu.png"/>
					<img class="img-margin-top" src="images/banners/Banner4-AllServices.png"/>
				</div>
				
				<div class="grBox">
					<h5>Header-401(k) Participant Education</h5>
					<p>We created an award winning interactive employee education course to help plan sponsors fulfill their responsibility to educate employees.</p>
					<p>Give it a try (Don’t forget to turn on your speakers).</p>
					<p>
						<a href="./videos/401k/index.html" target="_blank"><img class="img-margin-top video-img" src="./images/main-video.gif" border="0"/></a>
					</p>
					<img src="images/401KEducation-Icon.png" alt="education" />
					<h5>(Visit Our)</h5>
					<h4>401(k) Education Center</h4>
				</div>
				

			</div>
				<div class="extender"></div>
		</div>

		<script type="text/javascript">
			$(function () {
				$("#main-img").cycle({
					fx: 'fade',
					timeout: 10000
				});
			});
		</script>


<?php include 'includes/footer.php' ?>

			<div id="footer">
				<p class="small-text">
					This material is for informational purposes only. Retirement Plan Consultants, Inc does not provide investment, tax or legal advice. It is your responsibility to select and monitor your investment options to meet your retirement objectives. You may also want to consult your own independent advisor as to any investment, tax or legal statements made herein. 
					<br/><br/>
					Securities and advisory services offered through LPL Financial, a registered investment advisor, Member <a href="http://www.finra.org/">FINRA</a>/<a href="http://www.sipc.org/">SIPC</a>.
					Retirement Plan Consultants, Inc is a separate entity from LPL Financial. Please see Disclosures for more details.
					<br/><br/>
					NOT FDIC INSURED | MAY LOSE VALUE | NOT BANK GUARANTEED | NOT INSURED BY ANY GOVERNMENT AGENCY
					<br/><br/>
					This site is designed for U.S. residents only. The services offered within this site are available exclusively through our U.S. Investment Representatives.  The LPL Financial Registered Representatives associated with this site may only discuss and/or transact securities business with residents of the following states: AK, AZ, CA, CO, FL, ID, MI, NV, TX, UT, WA.
				</p>
			</div>
		</div>
	</div>
</body>
</html>
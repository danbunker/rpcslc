<?php
	include './header.php'
?>

<script language="JavaScript">
	$(document).ready(function() {
		// binds form submission and fields to the validation engine
		$( ".datepicker" ).datepicker({
					changeMonth: true,
					changeYear: true,
					yearRange: '1912:2012'
				});
		jQuery("#formID").validationEngine();
	});
	
	function checkLifeInsuranceAmount(field, rules, i, options){
		if ($("#life_insurance_elect").attr('checked') != "undefined" && $("#life_insurance_elect").attr('checked') == "checked" && $("#life_insurance_amount").val() == "") {
			return options.allrules.checkLifeInsuranceAmount.alertText;
		}
	}
	
	function checkPartnerLifeInsuranceAmount(field, rules, i, options){
		if ($("#partner_life_insurance_elect").attr('checked') != "undefined" && $("#partner_life_insurance_elect").attr('checked') == "checked" && $("#partner_life_insurance_amount").val() == "") {
			return options.allrules.checkLifeInsuranceAmount.alertText;
		}
	}
	function checkPartnerLifeInsuranceField(field, rules, i, options){
		if ($("#partner_life_insurance_elect").attr('checked') != "undefined" && $("#partner_life_insurance_elect").attr('checked') == "checked" && field.val() == "") {
			return options.allrules.required.alertText;
		}
	}
	function checkChildLifeInsuranceField(field, rules, i, options){
		if ($("#child_life_insurance_elect").attr('checked') != "undefined" && $("#child_life_insurance_elect").attr('checked') == "checked" && field.val() == "") {
			return options.allrules.required.alertText;
		}
	}

</script>

		<div id="content">
			<div="first-block">
			<table border="0" cellspacing="5" cellpadding="5" style="background-color: #5CB3FF; width: 100%;">
				<tr>
					<td valign="top"><h2 style="color: white;">Group Benefits - Elections must be made by 3/25/12</h2><h2>Academy Mortgage Corporation 
				Benefits Enrollment Form</h2></td>
					<td valign="top"><img src="/images/hartford_bi_alt_color_pos_jpg.JPG" height="150"/></td>
				</tr>
			</table>
			</div>
			<div id="second-block" style="background-color: #E8E1CA;">
					<form id="formID" action="enroll.php" class="formular" method="post" accept-charset="utf-8">
						<fieldset>
							<legend>
								Information About You
							</legend>
							<table border="0" cellspacing="5" cellpadding="5">
								<tr>
									<td valign="top">
										<label>
											<span>First Name: </span>
											<input value="" class="validate[required,maxSize[32],custom[onlyLetterSp]] text-input" type="text" name="first_name" id="first_name" />
										</label>
										<label>
											<span>Middle Name (Optional): </span>
											<input value="" class="validate[maxSize[30],custom[onlyLetterSp]] text-input" type="text" name="middle_name" id="middle_name" />
										</label>
										<label>
											<span>Last Name: </span>
											<input value="" class="validate[required,maxSize[32],custom[onlyLetterSp]] text-input" type="text" name="last_name" id="last_name" />
										</label>
										<label>
											<span>Date of Birth:</span>
											<input value="" class="validate[required] text-input datepicker" type="text" name="dob" id="dob" />
										</label>
										<label>
											<span>Email Address:</span>
											<input value="" class="validate[required,maxSize[50],custom[email]] text-input" type="text" name="email" id="email" />
										</label>
										<label>
											<span>Employee Daytime Phone (Work):</span>
											<input value="" class="validate[required,maxSize[32],custom[phone]] text-input" type="text" name="daytime_phone" id="daytime_phone" />
										</label>
										<label>
											<span>Employee Evening Phone (Home):</span>
											<input value="" class="validate[required,maxSize[32],custom[phone]] text-input" type="text" name="evening_phone" id="evening_phone" />
										</label>
										<label>
											<span>Expected Annual Earnings:</span>
											<input value="" class="validate[required,maxSize[14]] text-input" type="text" name="earnings" id="earnings" />
										</label>
									</td>
									<td valign="top">
										<label>
											<span>Social Security Number: </span>
											<input value="" class="validate[required,maxSize[12]] text-input" type="text" name="ssn" id="ssn" />
										</label>
										<!-- <label>
											<span>Employee ID Number: </span>
											<input value="" class="validate[required,maxSize[10]] text-input" type="text" name="eid" id="eid" />
										</label> -->
										<!-- <label>
											<span>Date of Hire: </span>
											<input value="" class="validate[required] text-input datepicker" type="text" name="hireDate" id="hireDate" />
										</label> -->
										<label>
											<span>Mailing Address: </span>
											<input value="" class="validate[required,maxSize[40]] text-input" type="text" name="address_1" id="address_1" />
										</label>
										<label>
											<span>City: </span>
											<input value="" class="validate[required,maxSize[50],custom[onlyLetterSp]] text-input" type="text" name="city" id="city" />
										</label>
										<label>
											<span>State (ex. UT, NY, CA): </span>
											<input value="" class="validate[required,maxSize[5],custom[onlyLetterSp]] text-input" type="text" name="state" id="state" />
										</label>
										<label>
											<span>Zip: </span>
											<input value="" class="validate[required,maxSize[10],custom[onlyLetterNumber]] text-input" type="text" name="zip" id="zip" />
										</label>
										<label>
											<span>Employee Gender: </span>
											<input class="validate[required] radio" type="radio" name="gender" id="male" value="M"/><span>Male: </span>
											<input class="validate[required] radio" type="radio" name="gender" id="female" value="F"/><span>Female: </span>
										</label>
									</td>
								</tr>
							</table>
						</fieldset>
						<fieldset>
							<legend>
								Voluntary Long Term Disability Insurance
							</legend>
							<p>
								You have the opportunity to enroll in Voluntary Long Term Disability Insurance.  Voluntary Long Term Disability Insurance helps to replace your 
								income if you are sick or injured and cannot work and is designed to begin after you have been disabled for a predetermined waiting period, 
								known as the elimination period, of 90 days.  This plan provides you with income protection to replace up to 60% of your Earnings, to a 
								maximum monthly benefit of $10,000. If you enroll during this enrollment period, your coverage is provided to you on a guaranteed issue basis 
								– no medical information is required. If you enroll after this enrollment period, evidence of insurability will be required for all coverage amounts.
							</p>
							<table border="0" cellspacing="0" cellpadding="0" style="border: 1px solid black;">
								<tr>
									<td style="border: 1px solid black; padding: 5px;"><strong>Age</strong></td>
									<td style="border: 1px solid black; padding: 5px;"> Under 25</td>
									<td style="border: 1px solid black; padding: 5px;">25-29</td>
									<td style="border: 1px solid black; padding: 5px;">30-34</td>
									<td style="border: 1px solid black; padding: 5px;">35-39 </td>
									<td style="border: 1px solid black; padding: 5px;">40-44</td>
									<td style="border: 1px solid black; padding: 5px;">45-49</td>
									<td style="border: 1px solid black; padding: 5px;">50-54</td>
									<td style="border: 1px solid black; padding: 5px;">55-59</td>
									<td style="border: 1px solid black; padding: 5px;">60-64</td>
									<td style="border: 1px solid black; padding: 5px;">65-69</td>
									<td style="border: 1px solid black; padding: 5px;">70-74</td>
									<td style="border: 1px solid black; padding: 5px;">75+</td>
								</tr>
								<tr>
									<td style="border: 1px solid black; padding: 5px;"><strong>Rate</strong></td>
									<td style="border: 1px solid black; padding: 5px;"> $0.080</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.1150</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.1450</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.2000</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.3300</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.5150</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.7050</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.8350</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.8550</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.7550</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.7550</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.7550</td>
								</tr>
							</table>
							<br/>
							<p>
							To calculate your semi-monthly cost, please use the following formula(s):
							<br/>
							_________ (My Annual Earnings (Maximum = $200,000)) / 12 = _________ (My Monthly Earnings) / 100 = _________ * _________ (rate) = _________ (semi monthly cost)
							</p>
							<label>
								<input class="validate[required] radio" type="radio" name="long_term_dis" id="long_term_dis_elect" value="purchase"/><span>I elect to <strong>purchase</strong> Long Term Disability coverage.</span>
								<br/>
								<input class="validate[required] radio" type="radio" name="long_term_dis" id="long_term_dis_decline" value="decline"/><span>I <strong>decline</strong> to purchase Long Term Disability coverage.</span>
							</label>
							
						</fieldset>
						<fieldset>
							<legend>
								Voluntary Life Insurance
							</legend>
							<p>
								You can purchase Voluntary Life Insurance in increments of $10,000. The maximum amount you can purchase cannot be more than 5 times 
								your annual Earnings or $500,000. If you elect an amount that exceeds the guaranteed issue amount of $150,000, you will need to provide 
								evidence of insurability that is satisfactory to The Hartford before the excess can become effective.
							</p>
							<table border="0" cellspacing="0" cellpadding="0" style="border: 1px solid black;">
								<tr>
									<td style="border: 1px solid black; padding: 5px;"><strong>Age</strong></td>
									<td style="border: 1px solid black; padding: 5px;"> Under 25</td>
									<td style="border: 1px solid black; padding: 5px;">25-29</td>
									<td style="border: 1px solid black; padding: 5px;">30-34</td>
									<td style="border: 1px solid black; padding: 5px;">35-39 </td>
									<td style="border: 1px solid black; padding: 5px;">40-44</td>
									<td style="border: 1px solid black; padding: 5px;">45-49</td>
									<td style="border: 1px solid black; padding: 5px;">50-54</td>
									<td style="border: 1px solid black; padding: 5px;">55-59</td>
									<td style="border: 1px solid black; padding: 5px;">60-64</td>
									<td style="border: 1px solid black; padding: 5px;">65-69</td>
									<td style="border: 1px solid black; padding: 5px;">70-74</td>
									<td style="border: 1px solid black; padding: 5px;">75+</td>
								</tr>
								<tr>
									<td style="border: 1px solid black; padding: 5px;"><strong>Rate</strong></td>
									<td style="border: 1px solid black; padding: 5px;"> $0.0150</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.0150 </td>
									<td style="border: 1px solid black; padding: 5px;"> $0.0150 </td>
									<td style="border: 1px solid black; padding: 5px;"> $0.0250</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.0350</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.0600</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.0950</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.1500</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.1950</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.3150</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.6900</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.9850</td>
								</tr>
							</table>
							<br/>
							<p>
							To calculate your semi-monthly cost, please use the following formula(s):
							<br/>
							_________ (Life Benefit Amount) / $1,000 = _________ * _________ (rate) = _________ (semi monthly cost)
							</p>
							<label>
								<input class="validate[required] radio" type="radio" name="life_insurance" id="life_insurance_elect" value="elect"/><span>I elect to <strong>purchase</strong> the total amount of Life coverage:</span>
								<input value="" class="validate[custom[number],funcCall[checkLifeInsuranceAmount]] text-input" type="text" name="life_insurance_amount" id="life_insurance_amount" />
								<br/>
								<input class="validate[required] radio" type="radio" name="life_insurance" id="life_insurance_decline" value="decline"/><span>I <strong>decline</strong> to purchase Life coverage.</span>
								<br/>
								<input class="validate[required] radio" type="radio" name="life_insurance" id="life_insurance_continue" value="continue"/><span> I already have Voluntary Life Coverage through Academy Mortgage and RMI, inc.  I elect to <strong>continue</strong> my current Life coverage.</span>
							</label>
							
						</fieldset>
						<fieldset>
							<legend>
								Spouse/Domestic Partner Voluntary Life Insurance
							</legend>
							<p>
								If you purchase Voluntary Life Insurance for yourself, you may elect Spouse/Domestic Partner Voluntary Life Insurance in increments of $5,000.  
								The maximum amount you can purchase cannot be more than the lesser of $250,000 or 50% of your Voluntary Life Insurance.  If you elect an 
								amount that exceeds the guaranteed issue amount of $50,000, your Spouse or Domestic Partner will need to provide evidence of insurability 
								that is satisfactory to The Hartford before the excess can become effective.
							</p>
							<p>
								Costs are based on Spouse/Domestic Partner’s age. 
							</p>
							<table border="0" cellspacing="0" cellpadding="0" style="border: 1px solid black;">
								<tr>
									<td style="border: 1px solid black; padding: 5px;"><strong>Age</strong></td>
									<td style="border: 1px solid black; padding: 5px;"> Under 25</td>
									<td style="border: 1px solid black; padding: 5px;">25-29</td>
									<td style="border: 1px solid black; padding: 5px;">30-34</td>
									<td style="border: 1px solid black; padding: 5px;">35-39 </td>
									<td style="border: 1px solid black; padding: 5px;">40-44</td>
									<td style="border: 1px solid black; padding: 5px;">45-49</td>
									<td style="border: 1px solid black; padding: 5px;">50-54</td>
									<td style="border: 1px solid black; padding: 5px;">55-59</td>
									<td style="border: 1px solid black; padding: 5px;">60-64</td>
									<td style="border: 1px solid black; padding: 5px;">65-69</td>
									<td style="border: 1px solid black; padding: 5px;">70-74</td>
									<td style="border: 1px solid black; padding: 5px;">75+</td>
								</tr>
								<tr>
									<td style="border: 1px solid black; padding: 5px;"><strong>Rate</strong></td>
									<td style="border: 1px solid black; padding: 5px;"> $0.0150</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.0150 </td>
									<td style="border: 1px solid black; padding: 5px;"> $0.0150 </td>
									<td style="border: 1px solid black; padding: 5px;"> $0.0250</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.0350</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.0600</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.0950</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.1500</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.1950</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.3150</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.6900</td>
									<td style="border: 1px solid black; padding: 5px;"> $0.9850</td>
								</tr>
							</table>
							<br/>
							<p>
							To calculate your semi-monthly cost, please use the following formula(s):
							<br/>
							_________ (Life Benefit Amount) / $1,000 = _________ * _________ (rate) = _________ (semi monthly cost)
							</p>
							<label>
								<input class="validate[required] radio" type="radio" name="partner_life_insurance" id="partner_life_insurance_elect" value="elect"/><span>I elect to <strong>purchase</strong> the total amount of Life coverage:</span>
								<input value="" class="validate[custom[number],funcCall[checkPartnerLifeInsuranceAmount]] text-input" type="text" name="partner_life_insurance_amount" id="partner_life_insurance_amount" />
								<br/>
								<input class="validate[required] radio" type="radio" name="partner_life_insurance" id="partner_life_insurance_decline" value="decline"/><span>I <strong>decline</strong> to purchase Life coverage.</span>
								<br/>
								<input class="validate[required] radio" type="radio" name="partner_life_insurance" id="partner_life_insurance_continue" value="continue"/><span> I already have Voluntary Life Coverage through Academy Mortgage and RMI, inc.  I elect to <strong>continue</strong> my current Life coverage.</span>
							</label>
							<label>
								<span>Spouse/Domestic Partner First Name: </span>
								<input value="" class="validate[maxSize[32],funcCall[checkPartnerLifeInsuranceField]] text-input" type="text" name="partner_first_name" id="partner_first_name" />
							</label>
							<label>
								<span>Spouse/Domestic Partner Middle Name: </span>
								<input value="" class="validate[maxSize[30],funcCall[checkPartnerLifeInsuranceField]] text-input" type="text" name="partner_middle_name" id="partner_middle_name" />
							</label>
							<label>
								<span>Spouse/Domestic Partner Last Name: </span>
								<input value="" class="validate[maxSize[32],funcCall[checkPartnerLifeInsuranceField]] text-input" type="text" name="partner_last_name" id="partner_last_name" />
							</label>
							<label>
								<span>Spouse/Domestic Partner Date of Birth:</span>
								<input value="" class="validate[funcCall[checkPartnerLifeInsuranceField]] text-input datepicker" type="text" name="partner_dob" id="partner_dob" />
							</label>
							<!-- <label>
								<span>Spouse/Domestic Partner Marriage Date:</span>
								<input value="" class="validate[funcCall[checkPartnerLifeInsuranceField]] text-input datepicker" type="text" name="partner_dom" id="partner_dom" />
							</label> -->
							<label>
								<span>Spouse/Domestic Partner Gender: </span>
								<input class="radio" type="radio" name="partner_gender" id="partner_male" value="M" checked="checked"/><span>Male: </span>
								<input class="radio" type="radio" name="partner_gender" id="partner_female" value="F"/><span>Female: </span>
							</label>
							<label>
								<span>Spouse/Domestic Partner Relationship: </span>
								<select name="partner_relationship" id="partner_relationship" class="">
									<option value="SP">Spouse</option>
									<option value="SGS">Same-gender Spouse</option>
									<option value="DP">Registered Domestic Partner</option>
								</select>
							</label>
						</fieldset>

						<fieldset>
							<legend>
								Child(ren) Voluntary Life Insurance
							</legend>
							<p>
								If you purchase Voluntary Life Insurance for yourself, you may elect Child(ren) Voluntary Life Insurance for your Dependent Child(ren) between 
								the ages of Live Birth and 26 years in the amount of $10,000.
							</p>
							<label>
								<input class="validate[required] radio" type="radio" name="child_life_insurance" id="child_life_insurance_elect" value="elect"/><span>I elect to <strong>purchase</strong> the total amount of $10,000 of Life coverage at a Semi-Monthly cost of $0.54 (cost is for all covered Children).</span>
								<br/>
								<input class="validate[required] radio" type="radio" name="child_life_insurance" id="child_life_insurance_decline" value="decline"/><span>I <strong>decline</strong> to purchase Life coverage.</span>
								<br/>
								<input class="validate[required] radio" type="radio" name="child_life_insurance" id="child_life_insurance_continue" value="continue"/><span> I already have Voluntary Life Coverage through Academy Mortgage and RMI, inc. I elect to <strong>continue</strong> my current Life coverage.</span>
							</label>

							<table border="0" cellspacing="5" cellpadding="5">
								<tr>
									<th>
										Child First Name
									</th>
									<th>
										Child Last Name
									</th>
									<th>
										Child Date of Birth
									</th>
									<th>
										Child Gender
									</th>
								</tr>
								<tr>
									<td>
										<label>
											<input value="" class="validate[maxSize[32],funcCall[checkChildLifeInsuranceField]] text-input" type="text" name="child_first_name_1" id="child_first_name_1" />
										</label>
									</td>
									<td>
										<label>
											<input value="" class="validate[maxSize[32],funcCall[checkChildLifeInsuranceField]] text-input" type="text" name="child_last_name_1" id="child_last_name_1" />
										</label>
									</td>
									<td>
										<label>
											<input value="" class="validate[funcCall[checkChildLifeInsuranceField]] text-input datepicker" type="text" name="child_dob_1" id="child_dob_1" style="width: 150px;"/>
										</label>
									</td>
									<td>
										<label>
											<input class="radio" type="radio" name="child_gender_1" id="child_gender_1_male" value="M" checked="checked"/><span>Male: </span>
											<input class="radio" type="radio" name="child_gender_1" id="child_gender_1_female" value="F"/><span>Female: </span>
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input value="" class="validate[maxSize[32],custom[onlyLetterSp]] text-input" type="text" name="child_first_name_2" id="child_first_name_2" />
										</label>
									</td>
									<td>
										<label>
											<input value="" class="validate[maxSize[32],custom[onlyLetterSp]] text-input" type="text" name="child_last_name_2" id="child_last_name_2" />
										</label>
									</td>
									<td>
										<label>
											<input value="" class="text-input datepicker" type="text" name="child_dob_2" id="child_dob_2"  style="width: 150px;"/>
										</label>
									</td>
									<td>
										<label>
											<input class="radio" type="radio" name="child_gender_2" id="child_gender_2_male" value="M"/><span>Male: </span>
											<input class="radio" type="radio" name="child_gender_2" id="child_gender_2_female" value="F"/><span>Female: </span>
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input value="" class="validate[maxSize[32],custom[onlyLetterSp]] text-input" type="text" name="child_first_name_3" id="child_first_name_3" />
										</label>
									</td>
									<td>
										<label>
											<input value="" class="validate[maxSize[32],custom[onlyLetterSp]] text-input" type="text" name="child_last_name_3" id="child_last_name_3" />
										</label>
									</td>
									<td>
										<label>
											<input value="" class="text-input datepicker" type="text" name="child_dob_3" id="child_dob_3"  style="width: 150px;"/>
										</label>
									</td>
									<td>
										<label>
											<input class="radio" type="radio" name="child_gender_3" id="child_gender_3_male" value="M"/><span>Male: </span>
											<input class="radio" type="radio" name="child_gender_3" id="child_gender_3_female" value="F"/><span>Female: </span>
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input value="" class="validate[maxSize[32],custom[onlyLetterSp]] text-input" type="text" name="child_first_name_4" id="child_first_name_4" />
										</label>
									</td>
									<td>
										<label>
											<input value="" class="validate[maxSize[32],custom[onlyLetterSp]] text-input" type="text" name="child_last_name_4" id="child_last_name_4" />
										</label>
									</td>
									<td>
										<label>
											<input value="" class="text-input datepicker" type="text" name="child_dob_4" id="child_dob_4" style="width: 150px;" />
										</label>
									</td>
									<td>
										<label>
											<input class="radio" type="radio" name="child_gender_4" id="child_gender_4_male" value="M"/><span>Male: </span>
											<input class="radio" type="radio" name="child_gender_4" id="child_gender_4_female" value="F"/><span>Female: </span>
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input value="" class="validate[maxSize[32],custom[onlyLetterSp]] text-input" type="text" name="child_first_name_5" id="child_first_name_5" />
										</label>
									</td>
									<td>
										<label>
											<input value="" class="validate[maxSize[32],custom[onlyLetterSp]] text-input" type="text" name="child_last_name_5" id="child_last_name_5" />
										</label>
									</td>
									<td>
										<label>
											<input value="" class="text-input datepicker" type="text" name="child_dob_5" id="child_dob_5" style="width: 150px;" />
										</label>
									</td>
									<td>
										<label>
											<input class="radio" type="radio" name="child_gender_5" id="child_gender_5_male" value="M"/><span>Male: </span>
											<input class="radio" type="radio" name="child_gender_5" id="child_gender_5_female" value="F"/><span>Female: </span>
										</label>
									</td>
								</tr>
							</table>
							
						</fieldset>
						<fieldset>
							<legend>
								Beneficiary Designation
							</legend>
							<p>
								You must select your beneficiary – the person (or more than one person) or legal entity (or more than one entity) who receives a benefit 
								payment if you die while covered by the plans. <strong>This beneficiary designation will be for ALL group life or accidental death insurance 
								coverage issued by The Hartford for you, unless specifically named otherwise.</strong> Please make sure that you also name a contingent 
								beneficiary – who would receive your benefit if your primary beneficiary dies first.
							</p>
							<p>
								Please make sure your beneficiary designation is clear so that there will be no question as to your meaning. If you name more than one primary 
								or contingent beneficiary, show the percentage of your benefit to be paid to each beneficiary. Please provide all of the information requested 
								below.  If your beneficiary is not related either by blood or by marriage, insert the words, “Not Related” as their stated relationship. If you need 
								assistance, contact your benefits administrator or your own legal advisor.
							</p>
							<h3>Primary Beneficiary</h3>
							<table border="0" cellspacing="5" cellpadding="5">
								<tr>
									<td valign="top">
										<label>
											<span>First Name: </span>
											<input value="" class="validate[required,maxSize[32],custom[onlyLetterSp]] text-input" type="text" name="prim_ben_first_name" id="prim_ben_first_name" />
										</label>
										<label>
											<span>Last Name: </span>
											<input value="" class="validate[required,maxSize[32],custom[onlyLetterSp]] text-input" type="text" name="prim_ben_last_name" id="prim_ben_last_name" />
										</label>
										<label>
											<span>Social Security Number: </span>
											<input value="" class="validate[required,maxSize[12]] text-input" type="text" name="prime_ben_ssn" id="prime_ben_ssn" />
										</label>
										<label>
											<span>Relationship: </span>
											<select name="prime_ben_relationship" id="prime_ben_relationship" class="">
												<option value="SP">Spouse</option>
												<option value="CH">Child</option>
												<option value="NR">Not Related</option>
												<option value="SGS">Same-gender Spouse</option>
												<option value="DP">Registered Domestic Partner</option>
											</select>
										</label>
										<label>
											<span>Date of Birth:</span>
											<input value="" class="validate[required] text-input datepicker" type="text" name="prime_ben_dob" id="prime_ben_dob" />
										</label>
									</td>
									<td valign="top">
										<label>
											<span>Mailing Address: </span>
											<input value="" class="validate[required,maxSize[40]] text-input" type="text" name="prim_ben_address_1" id="prim_ben_address_1" />
										</label>
										<label>
											<span>City: </span>
											<input value="" class="validate[required,maxSize[50],custom[onlyLetterSp]] text-input" type="text" name="prim_ben_city" id="prim_ben_city" />
										</label>
										<label>
											<span>State (ex. UT, NY, CA): </span>
											<input value="" class="validate[required,maxSize[5],custom[onlyLetterSp]] text-input" type="text" name="prim_ben_state" id="prim_ben_state" />
										</label>
										<label>
											<span>Zip: </span>
											<input value="" class="validate[required,maxSize[10],custom[onlyLetterNumber]] text-input" type="text" name="prim_ben_zip" id="prim_ben_zip" />
										</label>
										<label>
											<span>Beneficiary Percent (ex. 50): </span>
											<input value="" class="validate[required,maxSize[3],custom[onlyLetterNumber]] text-input" type="text" name="prim_ben_percent" id="prim_ben_percent" />
										</label>
									</td>
								</tr>
							</table>
							<h3>Second Primary Beneficiary (optional)</h3>
							<table border="0" cellspacing="5" cellpadding="5">
								<tr>
									<td valign="top">
										<label>
											<span>First Name: </span>
											<input value="" class="validate[maxSize[32],custom[onlyLetterSp]] text-input" type="text" name="prim_ben_first_name_2" id="prim_ben_first_name_2" />
										</label>
										<label>
											<span>Last Name: </span>
											<input value="" class="validate[maxSize[32],custom[onlyLetterSp]] text-input" type="text" name="prim_ben_last_name_2" id="prim_ben_last_name_2" />
										</label>
										<label>
											<span>Social Security Number: </span>
											<input value="" class="validate[maxSize[12]] text-input" type="text" name="prime_ben_ssn_2" id="prime_ben_ssn_2" />
										</label>
										<label>
											<span>Relationship: </span>
											<select name="prime_ben_relationship_2" id="prime_ben_relationship_2" class="">
												<option value="SP">Spouse</option>
												<option value="CH">Child</option>
												<option value="NR">Not Related</option>
												<option value="SGS">Same-gender Spouse</option>
												<option value="DP">Registered Domestic Partner</option>
											</select>
										</label>
										<label>
											<span>Date of Birth:</span>
											<input value="" class="text-input datepicker" type="text" name="prime_ben_dob_2" id="prime_ben_dob_2" />
										</label>
									</td>
									<td valign="top">
										<label>
											<span>Mailing Address: </span>
											<input value="" class="validate[maxSize[40]] text-input" type="text" name="prim_ben_address_1_2" id="prim_ben_address_1_2" />
										</label>
										<label>
											<span>City: </span>
											<input value="" class="validate[maxSize[50],custom[onlyLetterSp]] text-input" type="text" name="prim_ben_city_2" id="prim_ben_city_2" />
										</label>
										<label>
											<span>State (ex. UT, NY, CA): </span>
											<input value="" class="validate[maxSize[5],custom[onlyLetterSp]] text-input" type="text" name="prim_ben_state_2" id="prim_ben_state_2" />
										</label>
										<label>
											<span>Zip: </span>
											<input value="" class="validate[maxSize[10],custom[onlyLetterNumber]] text-input" type="text" name="prim_ben_zip_2" id="prim_ben_zip_2" />
										</label>
										<label>
											<span>Beneficiary Percent (ex. 50): </span>
											<input value="" class="validate[maxSize[3],custom[onlyLetterNumber]] text-input" type="text" name="prim_ben_percent_2" id="prim_ben_percent_2" />
										</label>
									</td>
								</tr>
							</table>
							<h3>Contingent Beneficiary (optional)</h3>
							<table border="0" cellspacing="5" cellpadding="5">
								<tr>
									<td valign="top">
										<label>
											<span>First Name: </span>
											<input value="" class="validate[maxSize[32],custom[onlyLetterSp]] text-input" type="text" name="cont_ben_first_name" id="cont_ben_first_name" />
										</label>
										<label>
											<span>Last Name: </span>
											<input value="" class="validate[maxSize[32],custom[onlyLetterSp]] text-input" type="text" name="cont_ben_last_name" id="cont_ben_last_name" />
										</label>
										<label>
											<span>Social Security Number: </span>
											<input value="" class="validate[maxSize[12]] text-input" type="text" name="cont_ben_ssn" id="cont_ben_ssn" />
										</label>
										<label>
											<span>Relationship: </span>
											<select name="cont_ben_relationship" id="cont_ben_relationship" class="">
												<option value="SP">Spouse</option>
												<option value="CH">Child</option>
												<option value="NR">Not Related</option>
												<option value="SGS">Same-gender Spouse</option>
												<option value="DP">Registered Domestic Partner</option>
											</select>
										</label>
										<label>
											<span>Date of Birth:</span>
											<input value="" class="text-input datepicker" type="text" name="cont_ben_dob" id="cont_ben_dob" />
										</label>
									</td>
									<td valign="top">
										<label>
											<span>Mailing Address: </span>
											<input value="" class="validate[maxSize[40]] text-input" type="text" name="cont_ben_address_1" id="cont_ben_address_1" />
										</label>
										<label>
											<span>City: </span>
											<input value="" class="validate[maxSize[50],custom[onlyLetterSp]] text-input" type="text" name="cont_ben_city" id="cont_ben_city" />
										</label>
										<label>
											<span>State (ex. UT, NY, CA): </span>
											<input value="" class="validate[maxSize[5],custom[onlyLetterSp]] text-input" type="text" name="cont_ben_state" id="cont_ben_state" />
										</label>
										<label>
											<span>Zip: </span>
											<input value="" class="validate[maxSize[10],custom[onlyLetterNumber]] text-input" type="text" name="cont_ben_zip" id="cont_ben_zip" />
										</label>
										<label>
											<span>Beneficiary Percent (ex. 50): </span>
											<input value="" class="validate[maxSize[3],custom[onlyLetterNumber]] text-input" type="text" name="cont_ben_percent" id="cont_ben_percent" />
										</label>
									</td>
								</tr>
							</table>
							<h3>Second Contingent Beneficiary (optional)</h3>
							<table border="0" cellspacing="5" cellpadding="5">
								<tr>
									<td valign="top">
										<label>
											<span>First Name: </span>
											<input value="" class="validate[maxSize[32],custom[onlyLetterSp]] text-input" type="text" name="cont_ben_first_name_2" id="cont_ben_first_name_2" />
										</label>
										<label>
											<span>Last Name: </span>
											<input value="" class="validate[maxSize[32],custom[onlyLetterSp]] text-input" type="text" name="cont_ben_last_name_2" id="cont_ben_last_name_2" />
										</label>
										<label>
											<span>Social Security Number: </span>
											<input value="" class="validate[maxSize[12]] text-input" type="text" name="cont_ben_ssn_2" id="cont_ben_ssn_2" />
										</label>
										<label>
											<span>Relationship: </span>
											<select name="cont_ben_relationship_2" id="cont_ben_relationship_2" class="">
												<option value="SP">Spouse</option>
												<option value="CH">Child</option>
												<option value="NR">Not Related</option>
												<option value="SGS">Same-gender Spouse</option>
												<option value="DP">Registered Domestic Partner</option>
											</select>
										</label>
										<label>
											<span>Date of Birth:</span>
											<input value="" class="text-input datepicker" type="text" name="cont_ben_dob_2" id="cont_ben_dob_2" />
										</label>
									</td>
									<td valign="top">
										<label>
											<span>Mailing Address: </span>
											<input value="" class="validate[maxSize[40]] text-input" type="text" name="cont_ben_address_1_2" id="cont_ben_address_1_2" />
										</label>
										<label>
											<span>City: </span>
											<input value="" class="validate[maxSize[50],custom[onlyLetterSp]] text-input" type="text" name="cont_ben_city_2" id="cont_ben_city_2" />
										</label>
										<label>
											<span>State (ex. UT, NY, CA): </span>
											<input value="" class="validate[maxSize[5],custom[onlyLetterSp]] text-input" type="text" name="cont_ben_state_2" id="cont_ben_state_2" />
										</label>
										<label>
											<span>Zip: </span>
											<input value="" class="validate[maxSize[10],custom[onlyLetterNumber]] text-input" type="text" name="cont_ben_zip_2" id="cont_ben_zip_2" />
										</label>
										<label>
											<span>Beneficiary Percent (ex. 50): </span>
											<input value="" class="validate[maxSize[3],custom[onlyLetterNumber]] text-input" type="text" name="cont_ben_percent_2" id="cont_ben_percent_2" />
										</label>
									</td>
								</tr>
							</table>
							
							<p>
								The beneficiary for insurance on the lives of your spouse/domestic partner and children will automatically be you, if surviving.  Otherwise, the 
								beneficiary will be the estate of the spouse/domestic partner and children, subject to policy provisions.  A beneficiary for employee Life 
								Insurance may be changed upon written request.	
							</p>
							<p>
								Spousal Consent For Community Property States Only:  If you live in a community property state – Arizona, California, Idaho, Louisiana, 
								Nevada, New Mexico, Texas, Washington, or Wisconsin – you may complete the Spousal Consent section, which allows your spouse/domestic 
								partner to waive his or her rights to any community property interest in the benefit.  Disclaimer: Spousal consent does not apply to ERISA plans.
							</p>
							<p>
								This will certify that, as spouse/domestic partner of the Employee named above, I hereby consent to my spouse/domestic partner designating 
								the person(s) listed above as beneficiaries of group life insurance under the above policy and waive any rights I may have to the proceeds of 
								such insurance under applicable community property laws.  I understand that this consent and waiver supersede any prior spousal consent or 
								waiver under this plan.
							</p>
							<p>
								Contact HR to request a spousal consent form.
							</p>
							<h3>Confirmation</h3>
							<p>
								I acknowledge that I have been given the opportunity to enroll in the Life and Disability insurance coverage described in the Benefit Highlight 
								Sheets and offered through Academy Mortgage Corporation.	
							</p>
							<p>
								I understand and agree that if I decline coverage now, but later decide to enroll, I will be required to provide evidence of insurability that is 
								satisfactory to The Hartford and be approved for such coverage before it becomes effective.  I understand my request for coverage may be 
								denied by The Hartford.
							</p>
							<p>
								I understand and agree that insurance will go into effect and remain in effect only in accordance with the provisions, terms and conditions of the 
								insurance policy.  I understand and agree that only the insurance policy issued to the policyholder (your employer) can fully describe the 
								provisions, terms, conditions, limitations and exclusions of your insurance coverage.  In the event of any difference between the enrollment form 
								and the insurance policy, I agree to be bound by the insurance policy.	
							</p>
							<p>
								If I have life insurance coverage with The Hartford, I understand and agree that my life insurance benefit is reduced at a specified age stated in 
								the policy.  If I have disability income coverage with The Hartford, I understand and agree that the maximum duration benefits are payable will 
								be limited to a specified period starting at a specified age and that a claim for benefits may not be approved for a pre-existing condition.	
							</p>
							<p>
							I authorize my employer to make the appropriate payroll deductions from my earnings.
								
							</p>
							<p>
								I understand that no insurance will be valid or in force if I am not eligible in accordance with the terms of the group policy as issued to my 
								employer.  I acknowledge and agree that if group participation requirements are not met, this policy will not be implemented and the coverage I 
								have elected will not be in force.
							</p>
							<p>
							By hitting submit I confirm that I understand these items.	
							</p>
						</fieldset>
						<input class="submit" type="submit" value="Submit"/><hr/>
					</form>
					<p style="font-size:10px;">
						The Hartford® is The Hartford Financial Services Group, Inc. and its subsidiaries, including issuing companies Hartford Life Insurance Company and Hartford 
						Life and Accident Insurance Company.  Policies sold in New York are underwritten by Hartford Life Insurance Company. Home Office of both companies: 
						Simsbury, CT.   All benefits are subject to the terms and conditions of the policy. Policies underwritten by the issuing companies listed above detail exclusions, 
						limitations, reduction of benefits and terms under which the policies may be continued in force or discontinued.	
					</p>
			</div>
<?php
	include './footer.php'
?>

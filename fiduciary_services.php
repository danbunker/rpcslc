<?php include 'includes/header.php' ?>
<?php include 'includes/mainNav.php' ?>

        <div id="corpus">
            <img src="images/inside_page_header.jpg" alt="banner" />
            <div id="content">
                <div class="tanBox" style="padding: 0;">
                    <div class="transBox fr editable" id="serviceBox">

                        <h5>Fiduciary Services</h5>
                        <p>Our mission is to "increase the retirement readiness of employees and employers of small businesses"! For this to occur the plan must be sound from a fiduciary standpoint and every opportunity for protection in the law should be taken. It all starts with a solid understanding of plan sponsor fiduciary responsibilities. Jere El-Bakri received the AIF (Accredited Investment Fiduciary) accreditation from FI 360. This accreditation required study, experience, testing, and classroom instruction at the Wharton School of Business campus in San Francisco. This knowledge, combined with Jere’s willingness to serve as a 3(21) fiduciary on many of the plans we advise, can help to focus on reducing a plan sponsors fiduciary exposure. Additionally, Jere has the ERPA (Enrolled Retirement Plan Agent) certification from the IRS. Maintaining this certification is very demanding and includes significant continuing education requirements.</p>
                        <p>Besides serving as a 3(21) fiduciary when possible, we also provide many other specific services to plan sponsors related to fiduciary risk. First, we create what we call a fiduciary binder. This binder holds important plan information like signed plan documents as amendments. It also serves as a living record of fiduciary committee meetings and ongoing changes to the plan. Having a concise record of sound fiduciary process is almost as important as the process itself. Second, we provide plan sponsor education on topics like recent law changes or best practices. Third, we provide fiduciary committee meetings that involve extensive investment monitoring and best practices that are all part of a process that helps plan sponsors to fulfill their responsibility to select and monitor the investment options in the plan. Fourth, we assist you in the preparation (and/or review) of an investment policy statement (IPS). Finally, we serve as a liaison with your compliance administrator to help insure the plan runs as outlined in your plan documents.Don’t let fears about fiduciary responsibilities stop you from offering a plan or serving on a committee for a plan. We are here to help.</p>



                    </div>
<?php include 'includes/servicesNav.php' ?>
                    <div class="extender"></div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(function () {
                $("#main-img").cycle({
                    fx: 'fade',
                    timeout: 10000
                });
            });
        </script>


<?php include 'includes/footer.php' ?>
